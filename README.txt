ABOUT
-----------------------------------------------------------------------------------------
The tapatio install profile makes it easy to set up a communications moderation
and dispatching system using twitter.

FEATURES
-----------------------------------------------------------------------------------------
* Tight integration with twitter.  
    Operators, users with access to the drupal system, associate twitter accounts 
    with organic groups.  All tweets from followers of those accounts will 
    automatically get pulled into the drupal system as nodes.

* Communications moderation
    Operators will then add and moderate posts, voteing on them (automatically dispatching
    at a configureable value), specifying priority levels, associating with other
    groups (twitter accounts), marking as a duplicate of another post, and dispatching.

* Detailed search / display interface
    Operators also have the abilty of searching for nodes by minutes since last recieved,
    wether or not the node has been dispatched, the current vote level, and wether or not
    the node has been prioritized.

* SMS Dispatching through twitter
    After a operator deems that a node is valid (ie. it has been verified by an alternate
    source, it has been assigned a SMS message, and it has been associated with a group)
    he or she can dispatch it.  This is essentially posting the sms message of the node
    as a tweet for each twitter account (group) that is associated with the node.

PREPARATION
-----------------------------------------------------------------------------------------

STEP 1...
  Download Drupal 5.x (http://drupal.org/download), and upload it on your server. 
  Don't install it yet, we first must prepare it for installation.  

  
  
STEP 2...
  Download the twitter API package from ARC90.  The lates release is available
  from http://lab.arc90.com/2008/06/php_twitter_api_client.php, and we keep a
  copy here:  http://comms.hackbloc.org/download.

  The package should be unpacked into the comms directory so that when you are
  done the directory structure will look like this:
  comms/
    Arc90_Service_Twitter/
      lib/
        Arc90/
          Service/
            TwitterSearch.php
            Twitter.php
            Twitter/
              Exception.php
              Response.php
  

  
  
STEP 3...
  Place the profile files in the directory 'profiles/tapatio'.
  
  
  
STEP 4...
  Download the following modules and unpack them in the directory 'profiles/tapatio/modules':
  Make sure that the module version is compatible with the Drupal version.
  
userpoints                http://drupal.org/project/userpoints
og                        http://drupal.org/project/og
views                     http://drupal.org/project/views
votingapi                 http://drupal.org/project/votingapi
comms                     http://drupal.org/project/tapatio

STEP 5...
  Configure your cron job (http://drupal.org/cron) to run  at least on the 
  minute.  The module is designed to pull content from twitter.com every time 
  the cron job runs.



INSTALLATION
-----------------------------------------------------------------------------------------
Make sure you have followed all the preparation steps, before installing Drupal.
If all your files are uploaded and in the right place, it's time to install Drupal.

Proceed with installation as you normally would. When asked for an installation 
profile, choose 'Tapatio'.

Please consult the handbooks (http://drupal.org/handbooks) if you need help installing Drupal.



CUSTOMIZATION AND POST-INSTALLATION
-----------------------------------------------------------------------------------------
Hopefully everything went smoothly and the install profile is running out of the box.
Most likely you will have to go and configure various settings before launching your site.

Comms specific settings can be configured by 'administer > Comms module settings'
